import axios from 'axios'

import { RootState } from '~/types'
import { FlightsState, Mutations, Actions, Getters } from '~/types/flights'

import { Flight } from '~/types/models/flights'

export const state = (): FlightsState => ({
  filters: {
    departureStation: '',
    arrivalStation: '',
    departureDate: '',
    returnDate: ''
  },
  outbound: [],
  inbound: []
})

export const mutations: Mutations<FlightsState> = {
  setFilters (state, payload) {
    state.filters = payload
  },
  setOutbound (state, payload) {
    state.outbound = payload
  },
  setInbound (state, payload) {
    state.inbound = payload
  },
  setReturnDate (state, payload) {
    state.filters.returnDate = payload
  }
}

export const actions: Actions<FlightsState, RootState> = {
  async fetchOutbound ({ commit, getters }) {
    commit('setOutbound', [])
    const flights = await axios.get(process.env.API_ENDPOINT + 'search', {
      params: getters.getOutboundParams
    })
    commit('setOutbound', flights.data)
  },
  async fetchInbound ({ state, commit, getters }, filters) {
    if (typeof filters === 'string') {
      commit('setReturnDate', filters)
    }
    commit('setInbound', [])
    if (state.filters !== undefined && state.filters.returnDate) {
      const flights = await axios.get(process.env.API_ENDPOINT + 'search', {
        params: getters.getInboundParams
      })
      commit('setInbound', flights.data)
    }
  },
  async fetchFlights ({ state, commit, dispatch }, filters) {
    commit('setFilters', filters)
    await dispatch('fetchOutbound')
    await dispatch('fetchInbound')
    return !!state.outbound.length
  }
}

export const getters: Getters<FlightsState, RootState> = {
  getDepartureDate (state) {
    return state.filters !== undefined ? state.filters.departureDate : ''
  },
  getReturnDate (state) {
    return state.filters !== undefined ? state.filters.returnDate : ''
  },
  getDepartureStation (state) {
    return state.filters !== undefined ? state.filters.departureStation : ''
  },
  getArrivalStation (state) {
    return state.filters !== undefined ? state.filters.arrivalStation : ''
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getOutboundParams (state, getters) {
    return {
      departureStation: getters.getDepartureStation,
      arrivalStation: getters.getArrivalStation,
      date: getters.getDepartureDate
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getInboundParams (state, getters) {
    return {
      departureStation: getters.getArrivalStation,
      arrivalStation: getters.getDepartureStation,
      date: getters.getReturnDate
    }
  },
  getOutbound (state) { return state.outbound.filter((item: Flight) => item.remainingTickets > 0) },
  getInbound (state) { return state.inbound.filter((item: Flight) => item.remainingTickets > 0) }
}
