import { RootState } from '~/types'
import { CartState, Mutations, Actions, Getters } from '~/types/cart'
import { CartItem } from '~/types/models/cart'

export const state = (): CartState => ({
  outbound: [],
  inbound: []
})

export const mutations: Mutations<CartState> = {
  setOutbound (state, payload) {
    state.outbound = []
    state.outbound.push(payload)
    localStorage.setItem('outbound', JSON.stringify(payload))
  },
  setInbound (state, payload) {
    state.inbound = []
    state.inbound.push(payload)
    localStorage.setItem('inbound', JSON.stringify(payload))
  },
  resetCart (state) {
    state.outbound = []
    localStorage.setItem('outbound', '')
    state.inbound = []
    localStorage.setItem('inbound', '')
  }
}

export const actions: Actions<CartState, RootState> = {
  addOutbound ({ commit }, payload) {
    commit('setOutbound', payload)
  },
  addInbound ({ commit }, payload) {
    commit('setInbound', payload)
  },
  resetCart ({ commit }) {
    commit('resetCart')
  }
}

export const getters: Getters<CartState, RootState> = {
  getCartItems (state) { return state.outbound.concat(state.inbound) },
  getTotal (state) {
    let total: number = 0
    state.outbound.concat(state.inbound).forEach((item: CartItem) => { total += item.price })
    return total
  },
  getInbound (state) {
    return state.inbound
  },
  getOutboundFareSellKey (state) {
    return state.outbound[0] ? state.outbound[0].fareSellKey : ''
  },
  getInboundFareSellKey (state) {
    return state.inbound[0] ? state.inbound[0].fareSellKey : ''
  }
}
