import { ActionTree } from 'vuex'

import { RootState, State } from '~/types'

export const state = (): State => ({})

export const actions: ActionTree<State, RootState> = {
  async nuxtServerInit ({ dispatch }, { req }) {
    const uri = req._parsedUrl.pathname.slice(1)
    await dispatch('stations/fetchStations')
    if (uri === 'search') {
      const params = req._parsedUrl.query
        .split('&')
        .map((param: string) => {
          return param.split('=')
        })
      await dispatch('flights/fetchFlights', {
        departureStation: params[0][1],
        arrivalStation: params[1][1],
        departureDate: params[2][1],
        returnDate: ''
      })
    }
  }
}
