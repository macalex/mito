import axios from 'axios'

import { RootState } from '~/types'
import { StationsState, Mutations, Actions, Getters } from '~/types/stations'

export const state = (): StationsState => ({
  stations: []
})

export const mutations: Mutations<StationsState> = {
  setStations (state, payload) {
    state.stations = payload
  }
}

export const actions: Actions<StationsState, RootState> = {
  async fetchStations ({ commit }) {
    const stations = await axios.get(process.env.API_ENDPOINT + 'asset/stations')
    commit('setStations', stations.data)
  }
}

export const getters: Getters<StationsState, RootState> = {
  getStations (state) { return state.stations }
}
