export default {
  mode: 'universal',
  env: {
    API_ENDPOINT: 'https://jsconf-ws-api.mito.hu/'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@assets/sass/main.sass'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/moment-module
    '@nuxtjs/moment',
    // Doc: https://vuetifyjs.com/en/getting-started/quick-start#nuxt-install
    ['@nuxtjs/vuetify', {
      theme: {
        themes: {
          light: {
            primary: '#0A008B',
            secondary: '#FFFFFF',
            accent: '#3434E0',
            error: '#C6007E',
            info: '#06038D',
            success: '#2bba15',
            warning: '#ffe819'
          }
        },
        dark: false
      }
    }],
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt/typescript
    '@nuxt/typescript-build'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  vuetify: {
    customVariables: ['~/assets/sass/_variables.sass'],
    defaultAccess: {
      font: {
        family: 'Roboto'
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
  }
}
