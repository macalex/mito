import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { Flight } from '~/types/models/flights'

export interface Filters {
  departureStation: string,
  arrivalStation: string,
  departureDate: string,
  returnDate: string
}

export interface FlightsState {
  filters: Filters,
  outbound: Flight[],
  inbound: Flight[],
}

export interface Mutations<S> extends MutationTree<S> {
  setFilters (state: S, payload: Filters): void,
  setOutbound (state: S, payload: Flight[]): void,
  setInbound (state: S, payload: Flight[]): void,
  setReturnDate (state: S, payload: string): void,
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchOutbound (context: ActionContext<S, R>): void,
  fetchInbound (context: ActionContext<S, R>, filter: string | undefined): void,
  fetchFlights (context: ActionContext<S, R>, filters: Filters): Promise<boolean>
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getOutbound (state: S, getters: Getters<S, R>, rootState: R): Flight[],
  getInbound (state: S, getters: Getters<S, R>, rootState: R): Flight[],
  getDepartureDate (state: S, getters: Getters<S, R>, rootState: R): string,
  getReturnDate (state: S, getters: Getters<S, R>, rootState: R): string,
  getDepartureStation (state: S, getters: Getters<S, R>, rootState: R): string,
  getArrivalStation (state: S, getters: Getters<S, R>, rootState: R): string
}
