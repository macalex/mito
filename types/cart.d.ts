import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { CartItem } from '~/types/models/cart'

export interface CartState {
  outbound: CartItem[],
  inbound: CartItem[],
}

export interface Mutations<S> extends MutationTree<S> {
  setOutbound (state: S, payload: CartItem): void,
  setInbound (state: S, payload: CartItem): void,
  resetCart (state: S): void,
}

export interface Actions<S, R> extends ActionTree<S, R> {
  addOutbound (context: ActionContext<S, R>, payload: CartItem): void,
  addInbound (context: ActionContext<S, R>, payload: CartItem): void,
  resetCart (context: ActionContext<S, R>): void,
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getCartItems (state: S, getters: Getters<S, R>, rootState: R): CartItem[],
  getInbound (state: S, getters: Getters<S, R>, rootState: R): CartItem[],
  getTotal (state: S, getters: Getters<S, R>, rootState: R): number | string,
  getOutboundFareSellKey (state: S, getters: Getters<S, R>, rootState: R): string,
  getInboundFareSellKey (state: S, getters: Getters<S, R>, rootState: R): string,
}
