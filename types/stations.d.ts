import { MutationTree, ActionTree, ActionContext, GetterTree } from 'vuex'
import { Station } from '~/types/models/stations'

export interface StationOption {
  text: string,
  value: string
}

export interface StationsState {
  stations: Station[],
}

export interface Mutations<S> extends MutationTree<S> {
  setStations (state: S, payload: Station[]): void,
}

export interface Actions<S, R> extends ActionTree<S, R> {
  fetchStations (context: ActionContext<S, R>): void,
}

export interface Getters<S, R> extends GetterTree<S, R> {
  getStations (state: S, getters: Getters<S, R>, rootState: R): Station[]
}
