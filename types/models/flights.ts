export interface Fare {
  fareSellKey: string,
  price: number,
  bundle: string
}

export interface Flight {
  carrierCode: string,
  flightNumber: string,
  remainingTickets: number,
  departure: Date,
  arrival: Date,
  fares: Fare[]
}
