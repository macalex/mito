export interface CartItem {
  flightNumber: string,
  fareSellKey: string,
  departureStation: string,
  arrivalStation: string,
  departure: string,
  arrival: string,
  price: number,
  bundle: string
}
