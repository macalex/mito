export interface Connection {
  iata: string,
  operationStartDate: Date,
  operationEndDate: Date
}

export interface Station {
  iata?: string,
  latitude?: number,
  longitude?: number,
  shortName?: string,
  connections?: Connection[]
}
